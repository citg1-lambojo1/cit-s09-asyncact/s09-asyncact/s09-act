package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
//This application will function as an endpoint that will be used in handling http request.
@RestController
//will require all routes within the class to use the set endpoint as part of its route
@RequestMapping("/greeting")
public class DiscussionApplication {
//	private ArrayList<Student> students = new ArrayList<String>();
	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	//localhost:8080/hello
	@GetMapping("/hello")
	//Maps a get request to the route "/hello" and invokes the method hello().
	public String hello(){
		return "Hello World";
	}
	// Route with String Query
	//localhost:8080/hi?name=value
	@GetMapping("/hi")
	//"@RequestParam" annotation that allow us to extract data from query string in URL
	public String hi(@RequestParam(value="name", defaultValue = "John") String name){
		return String.format("Hi %s", name);
	}

	//localhost
	@GetMapping("/friend")
	public String friend(@RequestParam(value="name", defaultValue = "Joe") String name, @RequestParam(value = "friend", defaultValue = "Jane") String friend){
		return String.format("Hello %s! My name is %s.", friend, name);
	}
	
	@GetMapping("/hello/{name}")
	public String greetFriend(@PathVariable("name") String name){
		return String.format("Nice to meet you, %s.", name);

	}

	//S09 - Async Activity
	//1. Create route /welcome ...
	@GetMapping("/welcome")
	public String welcome(@RequestParam(value="user", defaultValue = "Lambojo") String user, @RequestParam(value = "role", defaultValue = "student") String role) {
		switch (role) {
			case "admin":
				return String.format("Welcome back to the class portal, Admin %s!", user);
			case "teacher":
				return String.format("Thank you for logging in, Teacher %s!", user);
			case "student":
				return String.format("Welcome to the class portal, %s!", user);
			default:
				return String.format("Role out of range!");
		}

	}
	//2. create a /register route ...
	private ArrayList<Student> students = new ArrayList<>();
	@GetMapping("/register")
	public String register(@RequestParam(value="id", defaultValue = "123456789") String id, @RequestParam(value = "name", defaultValue = "Jane") String name, @RequestParam(value = "course", defaultValue = "BSIT") String course){
		Student student = new Student(name, id, course);
		students.add(student);
		return String.format("%s your ID number is registered on the system!", id);
	}

	private class Student {
		private String name;
		private String id;
		private String course;

		public Student(String name, String id, String course) {
			this.name = name;
			this.id = id;
			this.course = course;
		}
		public String getName() {
			return name;
		}

		public String getId() {
			return id;
		}

		public String getCourse() {
			return course;
		}
	}

	//3. create a /account/id with path variable
	@GetMapping("/account/{id}")
	public String account(@PathVariable("id") String id) {
		for (Student student : students) {
			if (student.getId().equals(id)) {
				return "Welcome back " + student.getName() + "! You are currently enrolled in " + student.getCourse() + ".";
			}
		}
		return "Your provided " + id + " is not found on the system!";
	}

	//Activity






}
